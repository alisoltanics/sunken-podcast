# sunken-podcast
Social media website for podcasts using django and vue.js
[sunken.pythonanywhere.com](https://sunken.pythonanywhere.com/)

## Installation

[Install Python 3.6.5](https://www.python.org/downloads/)

To install `requirements` simply run:

`pip install -r requirements.txt`

