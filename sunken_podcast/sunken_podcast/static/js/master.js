var profile = new Vue({
el: '#profile-head',
delimiters: ['[[', ']]'],
data: {
  address: address,
  info: '',
  show: false,
  edit: false,
  success: false,
  podcasts: '',
  liked_episodes: '',
  activePodcasts: true,
  activeFollowing: false,
  activeFollowers: false,
  activeLikes: false,
  error: false,
  fl: 'Follow',
  followClass: 'follow-btn',
  follows: false,
  followings: '',
  followers: '',
  permission: false,
  follow_per: false,
  myuser: '',
},
mounted () {
  pageid = window.location.pathname.split( '/' );
  username = pageid[2].replace('@','');
  axios
    .get(address+'/profile/api/')
    .then(response => {
      this.myuser = response.data.username;
    })
    .catch(function (error) {
        profile.logedin = false;
    });
  axios
    .get(address+'/podcast/api/user/checkfollow/'+username)
    .then(response => {
      this.follows = true;
    });
   axios
     .get(address+'/profile/api/'+username)
     .then(response => {
       this.info = response.data;
       this.show = true;
     })
     .catch(function (error) {
       if(error.response.statusText == 'Unauthorized'){
         window.location.href = address+"/login/";
       }
     });
   axios
     .get(address+'/podcast/api/user/list/'+username)
     .then(response => {
       this.podcasts = response.data;
     }
     );
  axios
    .get(address+'/podcast/api/user/unfollow/'+username)
      .then(response => {
        this.followinfo = response.data;
        this.fl = 'Unfollow';
        this.followClass = 'unfollow-btn';
      })
      .catch(function (error) {
      });
},
updated(){
  pageid = window.location.pathname.split( '/' );
  username = pageid[2].replace('@','');
  if (this.myuser == username) {
    this.permission = true;
  }else {
    this.follow_per = true;
  }
},
methods: {
  closeer: function(){
    this.error = false;
  },
  epUrl: function(id){
    return address+'/podcast/episode/'+id;
  },
  activepd: function(){
    this.activePodcasts = true;
    this.activeFollowing = false;
    this.activeFollowers = false;
    this.activeLikes = false;
  },
  close: function(){
    this.success = false;
     location.reload();
  },
   editInfo: function(){
     this.edit = !(this.edit);
   },
   attachFile(event) {
     if (event.target.files[0].length != 0) {
         this.picture = event.target.files[0];
     }
        },
  attachFile1(event) {
    if (event.target.files[0].length != 0) {
        this.cover = event.target.files[0];
    }
        },
   update: function(){
     axios.defaults.xsrfCookieName = 'csrftoken';
     axios.defaults.xsrfHeaderName = 'X-CSRFToken';
     pageid = window.location.pathname.split( '/' );
     username = pageid[2].replace('@','');
    const url = address+'/profile/api/'+username+'/';
    const bodyFormData = new FormData();
    bodyFormData.append('first_name', this.info.first_name);
    if (this.picture) {
      bodyFormData.append('picture', this.picture);
    }
    if (this.cover) {
      bodyFormData.append('cover', this.cover);
    }
    bodyFormData.append('last_name', this.info.last_name);
    bodyFormData.append('bio', this.info.bio);
    bodyFormData.append('birth_date', this.info.birth_date);
    bodyFormData.append('location', this.info.location);
    bodyFormData.append('url', this.info.url);
    axios.put(url,bodyFormData, { headers: { 'Content-Type': 'multipart/form-data' } })
    .then(response => {
      this.edit = !(this.edit);
      this.success = true;
    })
    .catch(function (error) {
        profile.error = true;
        profile.errorText = 'Something is wrong'
    });
    // {
    //   first_name:this.info.first_name,
    //   last_name: this.info.last_name,
    //   bio: this.info.bio,
    //   birth_date: this.info.birth_date,
    //   location: this.info.location,
    //   url: this.info.url,
    // },
  },
  likedEpisodes: function(){
    pageid = window.location.pathname.split( '/' );
    username = pageid[2].replace('@','');
    axios
    .get(address+'/podcast/api/likes/'+username)
    .then(response => {
      this.liked_episodes = response.data;
      this.activePodcasts = false;
      this.activeFollowing = false;
      this.activeFollowers = false;
      this.activeLikes = true;
    })
  },
  follow: function(){
      pageid = window.location.pathname.split( '/' );
      username = pageid[2].replace('@','');
      if (this.followClass != 'unfollow-btn') {
        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const bodyFormData = new FormData();
        bodyFormData.append('user_followed', username);
        axios
          .post(address+'/podcast/api/user/follow/',bodyFormData, { headers: { 'Content-Type': 'multipart/form-data' } })
          .then(
            response => {
              profile.followClass = 'unfollow-btn';
              this.fl = 'Unfollow';
            }
          )
          .catch(function (error) {
              console.log(error)
            });
      }else {
      axios.defaults.xsrfCookieName = 'csrftoken';
      axios.defaults.xsrfHeaderName = 'X-CSRFToken';
      axios
        .delete(address+'/podcast/api/user/unfollow/'+username)
        .then(
          response => {
            profile.followClass = 'follow-btn';
            this.fl = 'Follow';
          }
        )
        .catch(function (error) {
            console.log(error.response.request.response)
          });
    }
  },
  following: function(){
    pageid = window.location.pathname.split( '/' );
    username = pageid[2].replace('@','');
    axios
    .get(address+'/podcast/api/user/following/'+username)
    .then(response => {
      this.followings = response.data;
      this.activePodcasts = false;
      this.activeFollowing = true;
      this.activeFollowers = false;
      this.activeLikes = false;
    });
  },
  follower: function(){
    pageid = window.location.pathname.split( '/' );
    username = pageid[2].replace('@','');
    axios
    .get(address+'/podcast/api/user/follower/'+username)
    .then(response => {
      this.followers = response.data;
      this.activePodcasts = false;
      this.activeFollowing = false;
      this.activeFollowers = true;
      this.activeLikes = false;
    });
  },
  addfourl: function(username){
    return address+'/profile/@'+username;
  },
}
})
