var episode = new Vue({
  el: '#episode-audio',
  delimiters: ['[[', ']]'],
  data: {
    lk: 'Like',
    episode: '',
    likeinfo: '',
    setting: false,
    confirm: false,
    errors: false,
    success: false,
    podcast: '',
    likeClass: 'glyphicon glyphicon-heart eplike',
    once: true,
    likes: '',
    epuser: '',
    logedin: true,
    permission: false,
    cnt: 0,
  },
  mounted () {
    axios
      .get(address+'/profile/api/')
      .then(response => {
        this.info = response.data;
        this.myuser = this.info.username;
      })
      .catch(function (error) {
          episode.logedin = false;
      });
    pageid = window.location.pathname.split( '/' );
     axios
       .get(address+'/podcast/api/episodes/detail/'+pageid[3])
       .then(response => {
         this.episode = response.data;
         this.epuser = this.episode.username
         this.likes = this.episode.likes;
       });
    axios
      .get(address+'/podcast/api/episode/deletelike/'+pageid[3])
        .then(response => {
          this.likeinfo = response.data;
          this.lk = 'Unlike';
          this.likeClass = 'glyphicon glyphicon-heart epliked';
        })
  },
  updated(){
    if (this.once == true) {
      document.getElementById("ah").src=this.episode.audio_track;
      this.$refs.player.load();
    }
    this.addaudio();
    if (this.myuser == this.epuser) {
      this.permission = true;
    }
  },
  methods: {
    userUrl: function(username){
      return address+'/profile/'+username;
    },
    pcUrl: function(id){
      axios
        .get(address+'/podcast/api/'+id)
        .then(response => (this.podcast = response.data.name));
      return address+'/podcast/'+id
    },
    // for adding audio src
    addaudio: function(){
      if (this.once == true) {
        document.getElementById("ah").src=this.episode.audio_track;
        this.$refs.player.load();
        this.cnt = this.cnt+1;
        if (this.cnt>=3) {
          this.once = false;
        }
      }
    },
    // for adding audio src
    chsetting: function(){
      this.setting = !this.setting;
    },
    close: function(){
      this.success = false;
      this.errors = false;
    },
    chconfirm: function(){
      this.confirm = !this.confirm;
    },
    deleteep: function(){
      axios.defaults.xsrfCookieName = 'csrftoken';
      axios.defaults.xsrfHeaderName = 'X-CSRFToken';
       pageid = window.location.pathname.split( '/' );
       axios.delete(address+'/podcast/api/episodes/detail/'+pageid[3]);
       window.location.href = address+"/profile/"+this.myuser;
    },
    attachFile(event) {
      if (event.target.files[0].length != 0) {
          this.picture = event.target.files[0];
      }
    },
    update: function(){
      axios.defaults.xsrfCookieName = 'csrftoken';
      axios.defaults.xsrfHeaderName = 'X-CSRFToken';
      const bodyFormData = new FormData();
      bodyFormData.append('name', this.episode.name);
      if (this.picture) {
        bodyFormData.append('picture', this.picture);
      }
      bodyFormData.append('description', this.episode.description);
      bodyFormData.append('transcript', this.episode.transcript);
      pageid = window.location.pathname.split( '/' );
      axios
        .put(address+'/podcast/api/episodes/detail/'+pageid[3],bodyFormData, { headers: { 'Content-Type': 'multipart/form-data' } })
        .then(
          response => {
            this.episode = response.data;
            this.setting = !this.setting;
            this.success = true;
          }
        )
        .catch(function (error) {
            episode.errors = true;
          })
    },
    like: function(){
      if (this.likeClass != 'glyphicon glyphicon-heart epliked') {
        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const bodyFormData = new FormData();
        bodyFormData.append('episode', this.episode.id);
        axios
          .post(address+'/podcast/api/episode/addlike/',bodyFormData, { headers: { 'Content-Type': 'multipart/form-data' } })
          .then(
            response => {
              episode.likeClass = 'glyphicon glyphicon-heart epliked';
              this.lk = 'Unlike';
              pageid = window.location.pathname.split( '/' );
               axios
                 .get(address+'/podcast/api/episodes/detail/'+pageid[3])
                 .then(response => {
                   this.likes = response.data.likes;
                 });
            }
          )
      }else {
        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        axios
          .delete(address+'/podcast/api/episode/deletelike/'+this.episode.id)
          .then(
            response => {
              episode.likeClass = 'glyphicon glyphicon-heart eplike';
              this.lk = 'Like';
              pageid = window.location.pathname.split( '/' );
              axios
                .get(address+'/podcast/api/episodes/detail/'+pageid[3])
                .then(response => {
                  this.likes = response.data.likes;
                });
            }
          )
      }
    },
  }
})
