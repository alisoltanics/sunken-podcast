Vue.filter('snippet', function(value){
  return value.slice(0,70) + '...';
});
var profile = new Vue({
el: '#indexpage',
delimiters: ['[[', ']]'],
data: {
  episodeList: '',
  podcastList: '',
  url: '',
  previous: '',
  hasPrevious: false,
  hasNext: false,
  bothtrue: false,
  active: 'active-btns',
  lpbtns: 'lp-btns',
  h2lp: 'h2-lp',
  active1: 'active1',
  popular_episodes: '',
},
mounted () {
  axios
    .get(address+'/podcast/api/episodes/popular/')
    .then(response => {
      this.popular_episodes = response.data.results;
    });
  axios
    .get(address+'/profile/api/')
    .then(response => {
      this.info = response.data;
      this.myuser = this.info.username;
    });
    axios
      .get(address+'/podcast/api/episodes/list/')
      .then(response => {
        this.episodeList = response.data;
        if(this.episodeList.next != null){
          this.url = this.episodeList.next;
          this.hasNext = true;
        }
      });
      axios
        .get(address+'/podcast/api/podcasts/list/')
        .then(response => {
          this.podcastList = response.data;
        });
},
methods: {
  adduserUrl: function(username){
    return address+'/profile/@'+username;
  },
  addurlpod: function(id){
    return address+"/podcast/"+id;
  },
  addurl: function(id){
    return address+"/podcast/episode/"+id;
  },
  nextPage: function(){
    // this.show = false;
    setTimeout(() =>
    axios
      .get(this.url)
      .then(response => {
        this.episodeList = response.data;
        if(this.episodeList.next != null){
          this.url = this.episodeList.next;
          this.hasNext = true;
        }
        else {
          this.hasNext = false;
        }
        if(this.episodeList.previous != null){
          this.previous = this.episodeList.previous;
          this.hasPrevious = true;
        }
        else {
          this.hasPrevious = false;
        }
        if (this.hasPrevious == true && this.hasNext == true ) {
          this.bothtrue = true;
        }
        else {
          this.bothtrue = false;
        }
        // this.show = true;
      }), 000);
  },
  previousPage: function(){
    axios
      .get(this.previous)
      .then(response => {
        this.episodeList = response.data;
        if(this.episodeList.next != null){
          this.url = this.episodeList.next;
          this.hasNext = true;
        }
        else {
          this.hasNext = false;
        }
        if(this.episodeList.previous != null){
          this.previous = this.episodeList.previous;
          this.hasPrevious = true;
        }
        else {
          this.hasPrevious = false;
        }
        if (this.hasPrevious == true && this.hasNext == true ) {
          this.bothtrue = true;
        }
        else {
          this.bothtrue = false;
        }
      });
  },
},


})
