var basse = new Vue({
  el:'#navbar-1',
  delimiters: ['[[', ']]'],
  data: {
    info: '',
    myuser: '',
    logedin: true,
    show: false,
  },
  mounted(){
    axios
      .get(address+'/profile/api/')
      .then(response => {
        this.info = response.data;
        this.myuser = '    '+'Profile/'+'@'  +this.info.username+'    ';
        basse.show = true;
      })
      .catch(function (error) {
          basse.logedin = false;
          basse.show = true;
      });
  },
  methods: {
    addprurl: function(){
      return address+'/profile/@'+this.info.username;
    },
    logout: function(){
      axios.defaults.xsrfCookieName = 'csrftoken';
      axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const url = address+'/rest-auth/logout/';
        axios.post(url, { headers: { 'Content-Type': 'multipart/form-data' } })
        .then(
          response => {
            window.location.href = address+"/login/";
          });
    },
  },
})
