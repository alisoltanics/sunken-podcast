var addpodcast = new Vue({
  el:'#addpc-id',
  delimiters: ['[[', ']]'],
  data:{
    name: '',
    description: '',
    errortext: '',
    errors: false,
  },
  mounted(){
   axios
    .get(address+'/profile/api/')
    .then(response => {
      this.info = response.data;
      this.myuser = this.info.username
    });
  },
  methods: {
    close: function(){
      this.errors = false;
    },
    attachFile(event) {
      if (event.target.files[0].length != 0) {
          this.picture = event.target.files[0];
      }
    },
    addpodcast: function(){
      axios.defaults.xsrfCookieName = 'csrftoken';
      axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const url = address+'/podcast/api/create/';
        const bodyFormData = new FormData();
        bodyFormData.append('name', this.name);
        bodyFormData.append('description', this.description);
        bodyFormData.append('picture', this.picture);
        axios
          .post(url,bodyFormData, { headers: { 'Content-Type': 'multipart/form-data' } })
          .then(
            response => {
              window.location.href = address+"/profile/"+addpodcast.myuser;
            }
          )
          .catch(function (error) {
              addpodcast.errors = true;
              if (error.response.request.response == "{\"name\":[\"This field may not be blank.\"]}"){
                addpodcast.errortext = 'Name field may not be blank.';
              }
              else if (error.response.request.response == "{\"description\":[\"This field may not be blank.\"]}") {
                addpodcast.errortext = 'Description field may not be blank.';
              }
              else if (error.response.request.response == "{\"picture\":[\"The submitted data was not a file. Check the encoding type on the form.\"]}") {
                addpodcast.errortext = 'Please add a picture.';
              }
              else if (error.response.request.response == "{\"picture\":[\"Upload a valid image. The file you uploaded was either not an image or a corrupted image.\"]}") {
                addpodcast.errortext = 'Upload a valid image. The file you uploaded was either not an image or a corrupted image.';
              }
              else {
                addpodcast.errortext = 'Please fill in all fields.';
              }
            });
    },
  },
})
