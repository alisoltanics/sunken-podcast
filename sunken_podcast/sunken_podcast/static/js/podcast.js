var podcast = new Vue({
  el: '#podcast-detail',
  delimiters: ['[[', ']]'],
  data: {
    podcast: '',
    episodes: '',
    epnamedel: '',
    epdelid: '',
    setting: false,
    confirm: false,
    success: false,
    errors: false,
    confirmep: false,
    reported: false,
    permissionpc: false,
    myuser: '',
    loaded: false,
  },
  mounted () {
    pageid = window.location.pathname.split( '/' );
    axios
      .get(address+'/podcast/api/episodes/'+pageid[2])
      .then(response => {
        this.episodes = response.data;
        this.loaded = true;
      });
    axios
      .get(address+'/profile/api/')
      .then(response => {
        this.info = response.data;
        this.myuser = this.info.username;
      });
     axios
       .get(address+'/podcast/api/'+pageid[2])
       .then(response => {
         this.podcast = response.data;
         this.pcuser = response.data.username;
       });
  },
  updated(){
    if (this.myuser == this.pcuser) {
      this.permissionpc = true;
    }
  },
  methods: {
    adduserurl: function(username){
      return address+'/profile/@'+username;
    },
    hg() {
    setTimeout(() => this.reported = false, 2000);
    },
    report: function(episode_id){
      this.reported = true;
      axios.defaults.xsrfCookieName = 'csrftoken';
      axios.defaults.xsrfHeaderName = 'X-CSRFToken';
      const bodyFormData = new FormData();
      bodyFormData.append('episode', episode_id);
      axios
        .post(address+'/podcast/api/episodes/report/', bodyFormData, { headers: { 'Content-Type': 'multipart/form-data' } });
      this.hg();
    },
    chconfirmep: function(name, id){
      this.confirmep = !this.confirmep;
      this.epnamedel = name;
      this.epdelid = id;
    },
    deleteep: function(){
      pageid = window.location.pathname.split( '/' );
      axios.defaults.xsrfCookieName = 'csrftoken';
      axios.defaults.xsrfHeaderName = 'X-CSRFToken';
       axios.delete(address+'/podcast/api/episodes/detail/'+this.epdelid)
       .then(response => {
         this.confirmep = !this.confirmep;
         axios
           .get(address+'/podcast/api/episodes/'+pageid[2])
           .then(response => {
             this.episodes = response.data;
             this.countEpisodes = this.episodes.results.length;
           });
       });

    },
    epUrl: function(id){
      return address+'/podcast/episode/'+id
    },
    close: function(){
      this.success = false;
      this.errors = false;
    },
    chsetting: function(){
      this.setting = !this.setting;
    },
    chconfirm: function(){
      this.confirm = !this.confirm;
    },
    deletepc: function(){
      axios.defaults.xsrfCookieName = 'csrftoken';
      axios.defaults.xsrfHeaderName = 'X-CSRFToken';
       pageid = window.location.pathname.split( '/' );
       axios.delete(address+'/podcast/api/'+pageid[2])
       .then(
         response => {
           window.location.href = address+"/profile/"+this.myuser;
         }
       )
       .catch(function (error) {
           podcast.errors = true;
         });
    },
    attachFile(event) {
      if (event.target.files[0].length != 0) {
          this.picture = event.target.files[0];
      }
    },
    update: function(){
      axios.defaults.xsrfCookieName = 'csrftoken';
      axios.defaults.xsrfHeaderName = 'X-CSRFToken';
      const bodyFormData = new FormData();
      bodyFormData.append('name', this.podcast.name);
      if (this.picture) {
        bodyFormData.append('picture', this.picture);
      }
      bodyFormData.append('description', this.podcast.description);
      bodyFormData.append('user', this.podcast.user);
      bodyFormData.append('url', this.podcast.url);
      bodyFormData.append('id', this.podcast.id);
      pageid = window.location.pathname.split( '/' );
      axios
        .put(address+'/podcast/api/'+pageid[2],bodyFormData, { headers: { 'Content-Type': 'multipart/form-data' } })
        .then(
          response => {
            this.podcast = response.data;
            this.setting = !this.setting;
            this.success = true;
          }
        )
        .catch(function (error) {
            podcast.errors = true;
          })
    },
  },
})
