 var addepisode = new Vue({
   el: '#addep-id',
   delimiters: ['[[', ']]'],
   data: {
     errors: false,
     podcasts: '',
     podcast: '',
     name: '',
     description: '',
     transcript: '',
     errortext: '',
     info: '',
     myuser: '',
   },
   mounted () {
     axios
       .get(address+'/profile/api/')
       .then(response => {
         this.info = response.data;
         this.myuser = this.info.username;
         axios
           .get(address+'/podcast/api/user/list/'+this.myuser)
           .then(response => {
             this.podcasts = response.data.results;
           });
       });
   },
   methods: {
   close: function(){
     this.errors = false;
   },
   attachFile(event) {
     if (event.target.files[0].length != 0) {
         this.picture = event.target.files[0];
     }
   },
   attachFile1(event) {
     if (event.target.files[0].length != 0) {
         this.audio = event.target.files[0];
     }
   },
   addepisode: function(){
     axios.defaults.xsrfCookieName = 'csrftoken';
     axios.defaults.xsrfHeaderName = 'X-CSRFToken';
       const url = address+'/podcast/api/episodes/create/';
       const bodyFormData = new FormData();
       bodyFormData.append('name', this.name);
       bodyFormData.append('description', this.description);
       bodyFormData.append('transcript', this.transcript);
       bodyFormData.append('podcast', this.podcast);
       bodyFormData.append('picture', this.picture);
       bodyFormData.append('audio_track', this.audio);
       axios.post(url,bodyFormData, { headers: { 'Content-Type': 'multipart/form-data' } })
       .then(
         response => {
           window.location.href = address+"/podcast/"+this.podcast;
         }
       )
       .catch(function (error) {
           addepisode.errors = true;
           if (error.response.request.response == "{\"name\":[\"This field may not be blank.\"]}") {
             addepisode.errortext = 'Name field may not be blank.';
           }
           else if (error.response.request.response == "{\"description\":[\"This field may not be blank.\"]}") {
             addepisode.errortext = 'Description field may not be blank.';
           }
           else if (error.response.request.response == "{\"transcript\":[\"This field may not be blank.\"]}") {
             addepisode.errortext = 'Transcript field may not be blank.';
           }
           else if (error.response.request.response == "{\"podcast\":[\"This field may not be null.\"]}") {
             addepisode.errortext = 'Select a podcast.';
           }
           else if (error.response.request.response == "{\"picture\":[\"Upload a valid image. The file you uploaded was either not an image or a corrupted image.\"]}") {
             addepisode.errortext = 'Upload a valid image.';
           }
           else if (error.response.request.response == "{\"audio_track\":[\"Content-Type is not mpeg\"]}") {
             addepisode.errortext = 'Upload a valid audio.';
           }
           else if (error.response.request.response == "{\"audio_track\":[\"Audio file too large ( > 20mb )\"]}") {
             addepisode.errortext = 'Audio file is bigger than 20 Mb';
           }
           else if (error.response.request.response == "{\"audio_track\":[\"The submitted data was not a file. Check the encoding type on the form.\"]}") {
             addepisode.errortext = 'Upload an audio.';
           }
           else {
             addepisode.errortext = 'Some errors occurred.';
           }
        })
   },
   }

 })
