var login = new Vue({
  el:'#login-div-id',
  delimiters: ['[[', ']]'],
  data: {
    register1: false,
    username: '',
    password: '',
    errors: false,
    errortext: '',
    username1: '',
    email: '',
    password1: '',
    password2: '',
  },
  methods: {
    close: function(){
      this.errors = false;
    },
    registeron: function(){
      this.register1 = !this.register1;
    },
    login: function(){
      axios.defaults.xsrfCookieName = 'csrftoken';
      axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const url = address+'/rest-auth/login/';
        const bodyFormData = new FormData();
        bodyFormData.append('username', this.username);
        bodyFormData.append('password', this.password);
        axios.post(url,bodyFormData, { headers: { 'Content-Type': 'multipart/form-data' } })
        .then(
          response => {
            window.location.href = address+"/";
          }
        )
        .catch(function (error) {
            login.errors = true;
            if (error.response.request.response == '{"password":["This field may not be blank."]}'){
              login.errortext = 'Password field can not be blank';
            }
            else if (error.response.request.response == '{\"non_field_errors\":[\"Unable to log in with provided credentials.\"]}') {
              login.errortext = 'Username and password do not match. ';
            }
            else if (error.response.request.response == '{\"non_field_errors\":[\"Must include \\\"username\\\" and \\\"password\\\".\"]}') {
              login.errortext = 'You must enter an username. ';
            }
            else {
              login.errortext = 'Some errors occurred. ';
            }

          });
    },
    register: function(){
      axios.defaults.xsrfCookieName = 'csrftoken';
      axios.defaults.xsrfHeaderName = 'X-CSRFToken';
        const url = address+'/rest-auth/registration/';
        const bodyFormData = new FormData();
        bodyFormData.append('username', this.username1);
        bodyFormData.append('email', this.email);
        bodyFormData.append('password1', this.password1);
        bodyFormData.append('password2', this.password2);
        axios.post(url,bodyFormData, { headers: { 'Content-Type': 'multipart/form-data' } })
        .then(
          response => {
            window.location.href = address+"/";
          }
        )
        .catch(function (error) {
            login.errors = true;
            if (error.response.request.response == "{\"non_field_errors\":[\"The two password fields didn't match.\"]}"){
              login.errortext = 'The two password fields did not match.';
            }
            else if (error.response.request.response == "{\"email\":[\"Enter a valid email address.\"]}") {
              login.errortext = 'Enter a valid email address.';
            }
            else if (error.response.request.response == "{\"password1\":[\"This password is too short. It must contain at least 8 characters.\"]}") {
              login.errortext = 'This password is too short. It must contain at least 8 characters.';
            }
            else if (error.response.request.response == "{\"username\":[\"A user with that username already exists.\"]}") {
              login.errortext = 'A user with that username already exists.';
            }
            else if (error.response.request.response == "{\"email\":[\"A user is already registered with this e-mail address.\"]}") {
              login.errortext = 'A user is already registered with this e-mail address.';
            }
            else {
              login.errortext = 'Some errors occurred.';
            }
        })
    },
  },
})
