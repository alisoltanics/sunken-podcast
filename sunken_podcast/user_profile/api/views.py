from rest_framework import generics
from django.contrib.auth.models import User
from rest_framework.permissions import IsAuthenticated
from django.shortcuts import get_object_or_404
from django.core.exceptions import ValidationError
from user_profile.models import Profile
from .serializers import ProfileSerializer
from rest_framework.parsers import MultiPartParser, FormParser


class ProfileCUApi(generics.RetrieveUpdateAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    parser_classes = (MultiPartParser,)

    def get_object(self, **kwargs):
        queryset = self.get_queryset()
        user_id = User.objects.get(username = self.kwargs['username'])
        obj = get_object_or_404(queryset, user=user_id.id)
        return obj

    def perform_update(self, serializer):
        username = self.kwargs['username']
        if str(self.request.user) != str(username):
            raise ValidationError("you are not owner of this profile.")
        serializer.save(user=self.request.user)
################################################################################
class CurrentUserApi(generics.RetrieveAPIView):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    parser_classes = (MultiPartParser,)

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(queryset, user=self.request.user)
        return obj
