from django.urls import path
from .views import ProfileCUApi, CurrentUserApi


urlpatterns = [
    path('<username>/', ProfileCUApi.as_view(), name='profile-api'),
    path('', CurrentUserApi.as_view(), name='current-user-api'),
]
