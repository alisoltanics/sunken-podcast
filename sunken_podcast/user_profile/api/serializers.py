from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from user_profile.models import Profile

class ProfileSerializer(ModelSerializer):
    username = serializers.CharField(source='user.username', read_only=True)
    class Meta:
        model = Profile
        fields = ('first_name', 'last_name', 'username', 'bio', 'picture', 'cover', 'location', 'birth_date', 'url')
