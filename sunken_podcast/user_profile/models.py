from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.utils import timezone

class Profile(models.Model):
    ''' profile data model for users '''
    user = models.OneToOneField('auth.User', unique = True, on_delete = models.CASCADE)
    first_name = models.CharField(max_length = 74, blank = True, default='Your')
    last_name = models.CharField(max_length = 74, blank = True, default='Name')
    url = models.CharField(max_length = 400, blank = True, default='yoururl.com')
    birth_date = models.DateField(blank = True, null=True, default=timezone.now)
    location = models.CharField(max_length = 74, blank = True, default='Somewhere')
    bio = models.TextField(blank=True, default='Biography')
    picture = models.ImageField(upload_to="user_pics", blank=True, default="user_pics/defaults/user.png")
    cover = models.ImageField(upload_to="user_pics", blank=True, default="user_pics/defaults/gray-cover.png")

    def __str__(self):
        return '@' + str(self.user)
# actives user after signed up and creates a profile for him.
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        profile = Profile.objects.create(user = instance)
        user1 = User.objects.get(pk = instance.pk)
        user1.is_staff = True
        user1.is_active = True
        user1.save()
        profile.save()
################################################################################
