from django.shortcuts import render
from django.views.generic import TemplateView
from .models import Podcast


class PodcastDetailView(TemplateView):
    template_name = 'podcast/podcast_detail.html'
################################################################################
class EpisodeDetailView(TemplateView):
    template_name = 'podcast/episode_detail.html'
################################################################################
class IndexView(TemplateView):
    template_name = 'index.html'
################################################################################
class AddPodcastView(TemplateView):
    template_name = 'podcast/add_podcast.html'
################################################################################
class AddٍEpisodeView(TemplateView):
    template_name = 'podcast/add_episode.html'
################################################################################
