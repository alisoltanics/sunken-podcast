from django.urls import path
from .views import PodcastDetailView, EpisodeDetailView, AddPodcastView, AddٍEpisodeView


urlpatterns = [
    path('<pk>', PodcastDetailView.as_view(), name='podcast_detail'),
    path('episode/<pk>', EpisodeDetailView.as_view(), name='episode_detail'),
    path('add_podcast/', AddPodcastView.as_view(), name='add_podcast_pg'),
    path('add_episode/', AddٍEpisodeView.as_view(), name='add_episode_pg'),
]
 
