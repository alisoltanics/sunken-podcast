from django.urls import path
from .views import (PodcastListAPIView, PodcastRUDAPIView,
                    EpisodeListAPIView, EpisodeRUDAPIView,
                    PodcastCreateView, EpisodeCreateView,
                    EpisodeListAllAPIView, PodcastListAllAPIView,
                    AddLikeAPIView, LikeRetrieveDestroyAPIView,
                    LikeListAllAPIView, AddFollowAPIView,
                    FollowRetrieveDestroyAPIView, FollowRetrieveAPIView,
                    FollowingListAllAPIView, FollowerListAllAPIView,
                    PopularEpisodeListAPIView, ReportCreateView)


urlpatterns = [
    path('user/list/<username>', PodcastListAPIView.as_view(), name='podcast-list-api'),
    path('<pk>', PodcastRUDAPIView.as_view(), name='podcast_detail_api'),
    path('create/', PodcastCreateView.as_view(), name='podcast_create'),
    path('episodes/list/', EpisodeListAllAPIView.as_view(), name='episode-list-all-api'),
    path('episodes/report/', ReportCreateView.as_view(), name='episode-report-api'),
    path('episodes/popular/', PopularEpisodeListAPIView.as_view(), name='episode-list-popular-api'),
    path('podcasts/list/', PodcastListAllAPIView.as_view(), name='podcast-list-all-api'),
    path('likes/<username>', LikeListAllAPIView.as_view(), name='like-list-api'),
    path('episodes/<pk>', EpisodeListAPIView.as_view(), name='episode-list-api'),
    path('episodes/detail/<pk>', EpisodeRUDAPIView.as_view(), name='episode-detail-api'),
    path('episodes/create/', EpisodeCreateView.as_view(), name='episode-create'),
    path('episode/addlike/', AddLikeAPIView.as_view(), name='episode-addlike'),
    path('user/follow/', AddFollowAPIView.as_view(), name='follow'),
    path('user/following/<username>', FollowingListAllAPIView.as_view(), name='following'),
    path('user/follower/<username>', FollowerListAllAPIView.as_view(), name='follower'),
    path('user/checkfollow/<user_followed>/', FollowRetrieveAPIView.as_view(), name='checkfollow'),
    path('episode/deletelike/<episode_id>', LikeRetrieveDestroyAPIView.as_view(), name='episode-deletelike'),
    path('user/unfollow/<user_followed>', FollowRetrieveDestroyAPIView.as_view(), name='user-unfollow'),
]
