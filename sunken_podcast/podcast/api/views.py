from rest_framework import generics
from datetime import date, timedelta
from rest_framework.permissions import IsAuthenticated
from django.shortcuts import get_object_or_404
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from .serializers import (PodcastSerializer, EpisodeSerializer, PodcastCreateSerializer,
                          EpisodeListSerializer, PodcastListSerializer, LikeCreateSerializer,
                          FollowCreateSerializer, FollowingListSerializer,
                          ReportCreateSerializer,)
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination
from user_profile.models import Profile
from podcast.models import Podcast, Episode, Like, Follow, ReportEpisode


class PodcastListAPIView(generics.ListAPIView):
    serializer_class = PodcastSerializer

    def get_queryset(self):
        user_id = User.objects.get(username = self.kwargs['username'])
        queryset = Podcast.objects.filter(user_id=user_id.id)
        return queryset
################################################################################
class PodcastRUDAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = PodcastSerializer
    queryset = Podcast.objects.all()
    parser_classes = (MultiPartParser,)

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(queryset, pk=self.kwargs['pk'])
        return obj

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def perform_update(self, serializer):
        username = Podcast.objects.get(id=self.kwargs['pk']).user
        if str(self.request.user) != str(username):
            raise ValidationError("you are not owner of this Podcst.")
        serializer.save(user=self.request.user)

    def perform_destroy(self, serializer):
        username = Podcast.objects.get(id=self.kwargs['pk']).user
        if str(self.request.user) != str(username):
            raise ValidationError("you are not owner of this Podcst.")
        serializer.delete()
################################################################################
class PodcastCreateView(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = PodcastCreateSerializer
    parser_classes = (MultiPartParser,)

    def perform_create(self, serializer):
        print(self.request.user)
        serializer.save(user=self.request.user)
################################################################################
class EpisodeListAPIView(generics.ListAPIView):
    serializer_class = EpisodeSerializer

    def get_queryset(self):
        podcast_id = self.kwargs['pk']
        queryset = Episode.objects.filter(podcast = podcast_id)
        return queryset
################################################################################
class EpisodeRUDAPIView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = EpisodeSerializer
    queryset = Episode.objects.all()
    parser_classes = (MultiPartParser,)

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(queryset, pk=self.kwargs['pk'])
        return obj

    def put(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    def perform_update(self, serializer):
        username = Episode.objects.get(id=self.kwargs['pk']).user
        if str(self.request.user) != str(username):
            raise ValidationError("you are not owner of this episode.")
        serializer.save(user=self.request.user)

    def perform_destroy(self, serializer):
        username = Episode.objects.get(id=self.kwargs['pk']).user
        if str(self.request.user) != str(username):
            raise ValidationError("you are not owner of this Episode.")
        serializer.delete()
################################################################################
class EpisodeCreateView(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = EpisodeSerializer
    parser_classes = (MultiPartParser,)

    def perform_create(self, serializer):
        print(self.request.user)
        serializer.save(user=self.request.user)
################################################################################
class EpisodeListAllAPIView(generics.ListAPIView):
    serializer_class = EpisodeListSerializer
    queryset = Episode.objects.filter().order_by('-id')
    paginaion_class = LimitOffsetPagination
################################################################################
class PodcastListAllAPIView(generics.ListAPIView):
    serializer_class = PodcastListSerializer
    queryset = Podcast.objects.all().order_by('-id')[:8]
    paginaion_class = LimitOffsetPagination
################################################################################
class AddLikeAPIView(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = LikeCreateSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
################################################################################
class LikeRetrieveDestroyAPIView(generics.RetrieveDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = LikeCreateSerializer
    queryset = Like.objects.all()

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(queryset, episode=self.kwargs['episode_id'], user=self.request.user)
        return obj
################################################################################
class LikeListAllAPIView(generics.ListAPIView):
    serializer_class = EpisodeListSerializer
    paginaion_class = LimitOffsetPagination

    def get_queryset(self):
        user_id = User.objects.get(username = self.kwargs['username'])
        queryset1 = Like.objects.filter(user_id=user_id.id).order_by('-id')
        episodes_list = []
        for y in queryset1:
            episodes_list.append(y.episode_id)
        episodes = list(set(Episode.objects.filter(id__in=episodes_list).order_by('-like')))
        queryset = episodes
        return queryset
################################################################################
class AddFollowAPIView(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = FollowCreateSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
################################################################################
class FollowRetrieveDestroyAPIView(generics.RetrieveDestroyAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = FollowCreateSerializer
    queryset = Follow.objects.all()

    def get_object(self):
        queryset = self.get_queryset()
        obj = get_object_or_404(queryset, user_followed=self.kwargs['user_followed'], user=self.request.user)
        return obj
################################################################################
class FollowRetrieveAPIView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = FollowCreateSerializer
    queryset = Follow.objects.all()

    def get_object(self):
        queryset = self.get_queryset()
        user_id = User.objects.get(username = self.kwargs['user_followed'])
        obj = get_object_or_404(queryset, user_followed=self.request.user, user=user_id)
        return obj
################################################################################
class FollowingListAllAPIView(generics.ListAPIView):
    serializer_class = FollowingListSerializer
    paginaion_class = LimitOffsetPagination

    def get_queryset(self):
        user_id = User.objects.get(username = self.kwargs['username'])
        queryset1 = Follow.objects.filter(user_id=user_id.id).order_by('-id')
        following_list = []
        for y in queryset1:
            following_id = User.objects.get(username = y.user_followed)
            following_list.append(following_id.id)
        following = list(set(Profile.objects.filter(id__in=following_list)))
        queryset = following
        return queryset
################################################################################
class FollowerListAllAPIView(generics.ListAPIView):
    serializer_class = FollowingListSerializer
    paginaion_class = LimitOffsetPagination

    def get_queryset(self):
        queryset1 = Follow.objects.filter(user_followed=self.kwargs['username']).order_by('-id')
        follower_list = []
        for y in queryset1:
            follower_id = User.objects.get(id = y.user_id)
            follower_list.append(follower_id.id)
        follower = list(set(Profile.objects.filter(id__in=follower_list)))
        queryset = follower
        return queryset
################################################################################
class PopularEpisodeListAPIView(generics.ListAPIView):
    serializer_class = EpisodeListSerializer

    def get_queryset(self):
        episodes = list(Like.objects.filter().values_list('episode_id', flat=True))
        popular_ids = []
        for i in episodes:
            if episodes.count(i) >= 2:
                popular_ids.append(i)
        enddate = date.today()
        startdate = enddate - timedelta(days=6)
        popular_episodes = Episode.objects.filter(id__in=popular_ids).filter(date__range=[startdate, enddate])
        queryset = popular_episodes
        return queryset
################################################################################
class ReportCreateView(generics.CreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = ReportCreateSerializer
################################################################################
