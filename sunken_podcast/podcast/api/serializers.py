from django.core.exceptions import ValidationError
import os
from rest_framework.serializers import ModelSerializer, HyperlinkedIdentityField
from rest_framework import serializers
from user_profile.models import Profile
from podcast.models import Podcast, Episode, Like, Follow, ReportEpisode

class PodcastSerializer(ModelSerializer):
    username = serializers.CharField(source='user.username', read_only=True)
    url = HyperlinkedIdentityField(
    view_name = 'podcast_detail',
    lookup_field = 'pk'
    )
    episodes = serializers.SerializerMethodField('count_episodes')
    def count_episodes(self, id):
        return Episode.objects.filter(podcast=id).count()

    class Meta:
        model = Podcast
        fields = ('user', 'name', 'description', 'picture', 'url', 'id', 'episodes', 'username')
################################################################################
class EpisodeSerializer(ModelSerializer):
    username = serializers.CharField(source='user.username', read_only=True)
    likes = serializers.SerializerMethodField('count_likes')

    def count_likes(self, id):
        return Like.objects.filter(episode=id).count()

    class Meta:
        model = Episode
        fields = ('podcast', 'name', 'description', 'transcript', 'audio_track',
                  'picture', 'date', 'id', 'username', 'likes')

    def validate_audio_track(self, audio_track):
        if audio_track:
             if audio_track.size > 20*1024*1024:
                   raise ValidationError("Audio file too large ( > 20mb )")
             if not audio_track.content_type in ["audio/mpeg","audio/mp3"]:
                   raise ValidationError("Content-Type is not mpeg")
             if not os.path.splitext(audio_track.name)[1] in [".mp3",]:
                   raise ValidationError("Doesn't have proper extension")
        else:
             raise ValidationError("Couldn't read uploaded file")
        return audio_track
################################################################################
class PodcastCreateSerializer(ModelSerializer):
    class Meta:
        model = Podcast
        fields = ('name', 'description', 'picture', 'id')
################################################################################
class EpisodeListSerializer(ModelSerializer):
    username = serializers.CharField(source='user.username', read_only=True)
    podcast_name = serializers.CharField(source='podcast.name', read_only=True)
    class Meta:
        model = Episode
        fields = ('podcast', 'name', 'description', 'picture', 'date', 'id', 'user', 'username', 'podcast_name',)
################################################################################
class PodcastListSerializer(ModelSerializer):
    class Meta:
        model = Podcast
        fields = ('name', 'picture', 'id')
################################################################################
class LikeCreateSerializer(ModelSerializer):
    class Meta:
        model = Like
        fields = ('id', 'episode')
################################################################################
class FollowCreateSerializer(ModelSerializer):
    class Meta:
        model = Follow
        fields = ('id', 'user_followed')
################################################################################
class FollowingListSerializer(ModelSerializer):
    username = serializers.CharField(source='user.username', read_only=True)
    class Meta:
        model = Profile
        fields = ('first_name', 'last_name', 'username', 'bio', 'picture', 'cover', 'location', 'birth_date', 'url')
################################################################################
class ReportCreateSerializer(ModelSerializer):
    class Meta:
        model = ReportEpisode
        fields = ('episode',)
################################################################################
