from django.db import models
from django.contrib.auth.models import User
import datetime

class Podcast(models.Model):
    ''' Podcast data model '''
    user = models.ForeignKey('auth.User', on_delete = models.CASCADE)
    name = models.CharField(max_length = 100)
    description = models.TextField()
    picture = models.ImageField(upload_to="podcasts")

    def __str__(self):
        return self.name
################################################################################
class Episode(models.Model):
    ''' Episode data model '''
    user = models.ForeignKey('auth.User', on_delete = models.CASCADE)
    podcast = models.ForeignKey('Podcast', on_delete = models.CASCADE)
    name = models.CharField(max_length = 100)
    description = models.TextField()
    transcript = models.TextField()
    picture = models.ImageField(upload_to="Episodes")
    audio_track = models.FileField(upload_to="Episodes/audio")
    date = models.DateField(default = datetime.date.today)

    def __str__(self):
        return self.name
################################################################################
class Like(models.Model):
    ''' Like data model '''
    episode = models.ForeignKey('Episode', on_delete = models.CASCADE)
    user = models.ForeignKey('auth.User', on_delete = models.CASCADE)

    class Meta:
        unique_together = ('episode', 'user')

    def __str__(self):
        return str(self.user) + ' / ' + str(self.episode)
################################################################################
class Follow(models.Model):
    ''' Follow data model '''
    user_followed = models.CharField(max_length = 100)
    user = models.ForeignKey('auth.User', on_delete = models.CASCADE)

    class Meta:
        unique_together = ('user_followed', 'user')

    def __str__(self):
        return str(self.user) + ' follows ' + str(self.user_followed)
################################################################################
class ReportEpisode(models.Model):
    ''' Report data model '''
    episode = models.OneToOneField('Episode', on_delete = models.CASCADE)

    def __str__(self):
        return '"'+ str(self.episode) +'"' + ' reported'
################################################################################
