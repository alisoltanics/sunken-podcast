from django.contrib import admin
from .models import Podcast, Episode, Like, Follow, ReportEpisode
# Register your models here.

admin.site.register(Podcast)
admin.site.register(Episode)
admin.site.register(Like)
admin.site.register(Follow)
admin.site.register(ReportEpisode)
